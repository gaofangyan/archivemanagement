﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Model;
using BL;

public partial class EditArchiveBorrowRecord : System.Web.UI.Page
{
    ArchiveBorrowRecord archiveBorrowRecord = new ArchiveBorrowRecord();

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        ArchiveBorrowRecordBL archiveBorrowRecordBL = new ArchiveBorrowRecordBL();
        archiveBorrowRecord.BorrowerName = Convert.ToString(this.jyrxm.Text.Trim());
        archiveBorrowRecord.BorrowPurpose = Convert.ToString(this.jymd.Text.Trim());
        archiveBorrowRecord.BorrowerPhone = Convert.ToString(this.jyrdh.Text.Trim());
        archiveBorrowRecord.BorrowTime = Convert.ToDateTime(this.jysj.Text.Trim());
        archiveBorrowRecord.E_mail = Convert.ToString(this.jyryx.Text.Trim());
        archiveBorrowRecord.IfReturn = Convert.ToBoolean(this.sfgh.Text.Trim());
        archiveBorrowRecord.ReturnTime = Convert.ToDateTime(this.ghrq.Text.Trim());
        archiveBorrowRecord.Remarks = Convert.ToString(this.bz.Text.Trim());
        int count;
        count = archiveBorrowRecordBL.ModifyArchiveBorrowRecord(archiveBorrowRecord);
        if (count == 1)
        {
            Page.Server.Transfer("ViewArchiveBorrowRecord.apsx");
        }
        else
            ClientScript.RegisterStartupScript(this.GetType(),
                  "", "<script>alert('修改失败！');</script>"); return;
    }

    protected void Button2_Click(object sender, EventArgs e)
    {
        this.Dispose();
        Response.Redirect("ViewArchiveBorrowRecord.aspx");
    }
}