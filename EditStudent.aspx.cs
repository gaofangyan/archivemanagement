﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BL;
using Model;
using DB;

public partial class EditStudent : System.Web.UI.Page
{
    StudentBL studentBL = new StudentBL();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            StudentBind();
        }
    }

    public void StudentBind()
    {
        this.xsxh.DataBind();
        
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        Student student = new Student();
        
        //student.StudentID = Convert.ToString(this.xsxh.Text.Trim());
        student.StudentName = Convert.ToString(this.xsxm.Text.Trim());
        student.Sex = Convert.ToString(this.xsxb.Text.Trim());
        student.Passward = Convert.ToString(this.xsmm.Text.Trim());
        student.Nation = Convert.ToString(this.mz.Text.Trim());
        student.PoliticalStatus = Convert.ToString(this.xszzmm.Text.Trim());
        student.Profession = Convert.ToString(this.xszy.Text.Trim());
        student.Birstday = Convert.ToDateTime(this.sr.Text.Trim());
        student.EducationExperience = Convert.ToString(this.xsjyjl.Text.Trim());

        int count = 0;
        count = studentBL.ModifyStudent(student);
        if (count == 1)
        {
            Page.Server.Transfer("ViewStudent.aspx");
        }
        else
        {
            ClientScript.RegisterStartupScript(this.GetType(),
                    "", "<script>alert('更新失败！');</script>"); return;
        }
    }

    protected void Button2_Click(object sender, EventArgs e)
    {

    }
}