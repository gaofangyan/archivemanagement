﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Model;
using BL;

public partial class GetCurrentAdministrator : System.Web.UI.Page
{
    Administrator administrator = new Administrator();

    protected void Page_Load(object sender, EventArgs e)
    {
        AdministratorBL administratorBL = new AdministratorBL();
        string currentAdministrator = Session[administrator.AdministratorID].ToString();
        administratorBL.GetAdministrator(currentAdministrator);
    }
}