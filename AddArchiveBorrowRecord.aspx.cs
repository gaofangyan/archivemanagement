﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Model;
using BL;

public partial class AddArchiveBorrowRecord : System.Web.UI.Page
{
    //实例化档案借阅对象
    ArchiveBorrowRecord archiveBorrowRecord = new ArchiveBorrowRecord();

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        ArchiveBorrowRecordBL archiveBorrowRecordBL = new ArchiveBorrowRecordBL();
        /*string inputfileNumber = this.dabh.Text.Trim();
        string  fileNumber = archiveBorrowRecordBL.GetFileNumber();
        if (fileNumber == inputfileNumber)
        {
            ClientScript.RegisterStartupScript(this.GetType(),
                    "", "<script>alert('此档案已存在！');</script>"); return;
        }
        else
        {
        */
        archiveBorrowRecord.BorrowID = this.jybh.Text.Trim();
        archiveBorrowRecord.StudentID = this.xh.Text.Trim();
            archiveBorrowRecord.BorrowerName = this.jyrxm.Text.Trim();
            archiveBorrowRecord.BorrowerPhone = this.jyrdh.Text.Trim();
            archiveBorrowRecord.BorrowPurpose = this.jymd.Text.Trim();
            archiveBorrowRecord.BorrowTime = Convert.ToDateTime(this.jysj.Text.Trim());
            archiveBorrowRecord.E_mail= this.jyryx.Text.Trim();
            archiveBorrowRecord.IfReturn = Convert.ToBoolean(this.sfgh.Text.Trim());
            archiveBorrowRecord.ReturnTime = Convert.ToDateTime(this.ghrq.Text.Trim());
            archiveBorrowRecord.Remarks = this.bz.Text.Trim();

            int count = archiveBorrowRecordBL.AddArchiveBorrowRecord(archiveBorrowRecord);
            if (count == 1)
            {
                Page.Server.Transfer("ViewArchiveBorrowRecord.aspx");
            }
            else
                ClientScript.RegisterStartupScript(this.GetType(),
                        "", "<script>alert('添加失败！');</script>"); return;
    
    }

    protected void Button2_Click(object sender, EventArgs e)
    {
        this.Dispose();
        Response.Redirect("Default.aspx");
    }
}