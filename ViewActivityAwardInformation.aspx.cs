﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BL;
using Model;

public partial class ViewActivityAwardInformation : System.Web.UI.Page
{
    ActivityAwardInformationBL activityAwardInformationBL = new ActivityAwardInformationBL();
    ActivityAwardInformation activityAwardInformation = new ActivityAwardInformation();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindActivityAwardInformation();
        }
    }

    protected void BindActivityAwardInformation()
    {
        this.GridView1.DataSource = activityAwardInformationBL.GetActivityAwardInformation();
        this.GridView1.DataBind();
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Attributes.Add("onmouseover", "this.style.backgroundcolor = currentcolor");
            ImageButton imgbtn = (ImageButton)e.Row.FindControl("imgbtnDelete");
            imgbtn.Attributes.Add("onclick", "return confirm('确认删除吗？');");
        }
    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        string cmd = e.CommandName;
        string  id = Convert.ToString(e.CommandArgument);
        if (cmd == "De")
        {
            activityAwardInformationBL.DeleteActivityAwardInformationByID(id);
        }
        else if (cmd == "Ed")
        {
            Page.Server.Transfer("EditActivityAwardInformation.aspx? AwardID=" + id.ToString());
        }
        BindActivityAwardInformation();
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        string inputStudentID = this.xh.Text.Trim();
        //activityAwardInformation.FileNumber= Convert.ToString(this.dabh.Text.Trim());
        activityAwardInformationBL.GetActivityAwardInformationByStudentID(this.xh.Text.Trim());
        BindActivityAwardInformation();
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        BindActivityAwardInformation();
    }
}