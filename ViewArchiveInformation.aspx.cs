﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BL;
using Model;

public partial class ViewArchiveInformation : System.Web.UI.Page
{
    ArchiveInformationBL archiveInformationBL = new ArchiveInformationBL();
    ArchiveInformation archiveInformation = new ArchiveInformation();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindArchiveInformation();
        }
    }

    //绑定档案信息
    protected void BindArchiveInformation()
    {
        this.GridView1.DataSource = archiveInformationBL.GetArchiveInformation();
        this.GridView1.DataBind();
    }

    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    //执行数据行按钮事件
    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        string cmd = e.CommandName;
        string studentID =  Convert.ToString(e.CommandArgument); //获取命令参数
        if (cmd == "De")
        {
            archiveInformationBL.DeleteArchiveInformationByFileNumber(studentID);
        }
        else if(cmd=="Ed")
        {
            Page.Server.Transfer("EditArchiveInformation.aspx? StudentID=" + studentID.ToString());
        }
        else if (cmd == "View")
        {
            Page.Server.Transfer("ViewActivityAwardInformation.aspx? StudentID=" + studentID.ToString());
        }
        BindArchiveInformation();
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        string studentID = archiveInformationBL.GetFileNumberByStudentID(archiveInformation.Student.StudentID);
        string inputStudentID = this.xh.Text.Trim(); 
        if (inputStudentID ==studentID )
        { 
            this.GridView1.DataSource=archiveInformationBL.GetArchiveInformationByStudentID(inputStudentID);
            this.GridView1.DataBind();
        }
        else
        {
            ClientScript.RegisterStartupScript(this.GetType(),
                   "", "<script>alert('此档案不存在！');</script>"); return;
        }
       
    }

    protected void xh_TextChanged(object sender, EventArgs e)
    {

    }

    //执行gridview数据行绑定事件
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Attributes.Add("onmouseover", "this.style.backgroundcolor = currentcolor");
            ImageButton imgbtn = (ImageButton)e.Row.FindControl("imgbtnDelete");
            imgbtn.Attributes.Add("onclick", "return confirm('确认删除吗？');");
        }
        
    }
    //执行分页
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        BindArchiveInformation();
    }
}