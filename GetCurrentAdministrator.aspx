﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="GetCurrentAdministrator.aspx.cs" Inherits="GetCurrentAdministrator" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>海南大学学生档案管理系统</title>
    <style type="text/css">
        .mainbody
        {
            height:100%;
            width: 100%;
            background-color:#E6E6FA;
        }
        #bodyDiv
        {
            height:100%;
            width: 100%;
        }
        .head
        {
            width: 100%;
            height: 35px;
        }
        
        .top
        {
            height: 41px;
            width: 100%;
        }
        #table1
        {
            height: 72px;
            width: 517px;
            margin-right: 614px;
            margin-bottom: 9px;
            margin-left:490px;
        }
        .logo
        {
            height: 135px;
            width:100%;
           background-color:#B2DFEE;
        }
              .style4
        {
            width: 593px;
        }
        .style6
        {
            width: 500px;
        }
              .style7
        {
            width: 118px;
        }
        .style8
        {
            width: 204px;
        }
              .style9
        {
            width: 317px;
        }
              </style>
</head>
<body class="mainbody">
    <form id="form1" runat="server"  method="post" action="GetCurrentAdministrator.aspx">
    <div id="bodyDiv">
        <div class="logo">
            <h2 ><img src="picture/logo_school.png" style="height: 70px" />
                <img src="picture/logo2.png" 
                    style="margin-left: 442px; height: 72px; width: 372px;" />
                      
            </h2>
             <span id="Label3" style="font-size:x-large; color:Black">&nbsp;欢迎您：</span>    
                                  
        </div>
       <hr style="height: 7px; width:100%; background-color:#B2DFEE" />
           <table>
              <tr>
                <td class="style8" ><a class ='top_link ' href='Default.aspx' style="font-size:x-large; color:Olive">返回首页</a></td>
                <td class="style6" ><span id="Label4" style="font-size:x-large; color:Olive">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 当前位置： </span></td>
                <td class="style4" ><span id="Label5"  style="font-size:x-large; color:Olive">&nbsp;个人信息</span></td>
                <td class="style7">&nbsp;&nbsp; <a id="likTc" href="Login.aspx"">退出</a></td>
            </tr>
          </table>
            <hr style="height: 2px; width:100%; background-color:#B2DFEE" />
        <br />
        <br /><br />

        <table border="2px" style="width: 409px; height: 229px; margin-left: 350px">
           <tr>
              <td>管理员编号:</td><td><asp:TextBox ID="glybh" runat="server" ></asp:TextBox></td>
           </tr>
            
            <tr>
              <td>管理员姓名:</td><td><asp:TextBox ID="glyxm" runat="server" ></asp:TextBox></td>
            </tr>
            <tr>
              <td>密 码：</td><td><asp:TextBox ID="mm" runat="server" ></asp:TextBox></td>
            </tr>
            <tr>
             <td>性别：</td><td><asp:TextBox ID="xb" runat="server" ></asp:TextBox></td>
            </tr>
            <tr>
              <td>邮箱：</td><td><asp:TextBox ID="TextBox1" runat="server" ></asp:TextBox></td>
            </tr>
            
       </table>
    </div>
    </form>
</body>
</html>

