﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ModifyPassward.aspx.cs" Inherits="ModifyPassward" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>海南大学学生档案管理系统</title>
    <style type="text/css">
        .mainbody
        {
            height:100%;
            width: 100%;
            background-color:#E6E6FA;
        }
        #bodyDiv
        {
            height:100%;
            width: 100%;
        }
        .head
        {
            width: 100%;
            height: 35px;
        }
        
        .top
        {
            height: 41px;
            width: 100%;
        }
        #table1
        {
            height: 72px;
            width: 400px;
            margin-right: 614px;
            margin-bottom: 9px;
            margin-left:490px;
        }
        .logo
        {
            height: 135px;
            width:100%;
           background-color:#B2DFEE;
        }
              .style4
        {
            width: 593px;
        }
        .style6
        {
            width: 500px;
        }
              .style7
        {
            width: 118px;
        }
        .style8
        {
            width: 204px;
        }
              .style10
        {
            width: 209px;
        }
        .style11
        {
            width: 88px;
        }
              </style>
</head>
<body class="mainbody">
    <form id="form1" runat="server"  method="post" action="ModifyPassward.aspx">
    <div id="bodyDiv">
        <div class="logo">
            <h2 ><img src="picture/logo_school.png" style="height: 70px" />
                <img src="picture/logo2.png" 
                    style="margin-left: 442px; height: 72px; width: 372px;" />
            </h2>
            <span id="Label3" style="font-size:x-large; color:Black">&nbsp;欢迎您：</span>       
             <asp:Label ID="glyxm" runat="server" Text="Label" Font-Size="Large" BackColor="#B2DFEE"></asp:Label>&nbsp; 管理员                       
        </div>
       <hr style="height: 7px; width:100%; background-color:#B2DFEE" />
           <table>
              <tr>
                <td class="style8" ><a class ='top_link ' href='Default.aspx' style="font-size:x-large; color:Olive">返回首页</a></td>
                <td class="style6" ><span id="Label4" style="font-size:x-large; color:Olive">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 当前位置： </span></td>
                <td class="style4" ><span id="Label5"  style="font-size:x-large; color:Olive">&nbsp;密码修改</span></td>
                <td class="style7">&nbsp;&nbsp; <a id="likTc" href="Login.aspx"">退出</a></td>
            </tr>
          </table>
            <hr style="height: 2px; width:100%; background-color:#B2DFEE" />
        <br />
        <br /><br />

        <table style="height: 285px; width: 552px; margin-left: 412px; background-color:#B2DFEE">
           <tr>
            <td class="style11">用户名:</td><td><asp:TextBox ID="glybh" runat="server" 
                   Width="178px" Height="29px" 
                style="margin-left: 21px" ontextchanged="glybh_TextChanged1"></asp:TextBox></td>
                <td class="style10"><asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="glybh"
                 ErrorMessage="请输入用户名" ForeColor="#FF66CC"></asp:RequiredFieldValidator></td>
            </tr>
            <tr> 
               <td class="style11">旧密码:</td><td><asp:TextBox ID="txtOldPwd" runat="server" Width="178px" Height="29px" 
                style="margin-left: 21px"></asp:TextBox></td>
                <td class="style10"><asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtOldPwd"
                 ErrorMessage="请输入旧密码" ForeColor="#FF66CC"></asp:RequiredFieldValidator></td>
            </tr>
            <tr> 
               <td class="style11">新密码:</td><td><asp:TextBox ID="txtNewPwd" runat="server" Width="178px" Height="29px" 
                style="margin-left: 21px"></asp:TextBox></td>
                <td class="style10"><asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtNewPwd"
                 ErrorMessage="请输入新密码" ForeColor="#FF66CC"></asp:RequiredFieldValidator></td>
            </tr>
             <tr>
               <td class="style11">新密码:</td><td> <asp:TextBox ID="txtNewPwdAgain" 
                     runat="server" Width="178px" Height="29px" 
                style="margin-left: 21px"></asp:TextBox></td>
                <td class="style10"><asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="txtNewPwd"
                 ControlToValidate="txtNewPwdAgain" ErrorMessage="请保持新密码两次输入一致" ForeColor="#FF66FF"></asp:CompareValidator></td>
            </tr>
             <tr>
              <td align="center" height="50px" class="style11">
               <asp:Button ID="Button1" runat="server" Text="确定" Font-Size="Large" 
                ForeColor="Blue" onclick="Button1_Click" /></td>
              <td class="style2" align="right" >
               <asp:Button ID="Button2" runat="server" Text="重置" Font-Size="Large" 
                ForeColor="Blue" onclick="Button2_Click" style="margin-left: 0px;"  /></td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
