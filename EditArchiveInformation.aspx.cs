﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BL;
using Model;

public partial class EditArchiveInformation : System.Web.UI.Page
{
    
    ArchiveInformation archiveInformation = new ArchiveInformation();
    protected void Page_Load(object sender, EventArgs e)
    {
       
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        ArchiveInformationBL archiveInformationBL = new ArchiveInformationBL();

        //archiveInformation.FileNumber = Convert.ToString(this.dabh.Text.Trim());
        //archiveInformation.Student.StudentID = Convert.ToString(this.xh.Text.Trim());
        archiveInformation.ActivityAwardInformation[0].AwardID = Convert.ToString(this.jlbh.Text.Trim());
        archiveInformation.ArchiveBorrowRecord[0].BorrowID = Convert.ToString(this.jybh.Text.Trim());
        archiveInformation.Address = Convert.ToString(this.cfwz.Text.Trim());
        archiveInformation.TransferTime = Convert.ToDateTime(this.drsj.Text.Trim());

        int count;
        count=archiveInformationBL.ModifyArchiveInformation(archiveInformation);
        if (count == 1)
        {
            Page.Server.Transfer("ViewArchiveInformation.aspx");
        }
        else
        {
            ClientScript.RegisterStartupScript(this.GetType(),
                  "", "<script>alert('修改失败！');</script>"); return;
        }
    }

    protected void Button2_Click(object sender, EventArgs e)
    {

    }

    protected void dabh_TextChanged(object sender, EventArgs e)
    {

    }

    protected void xh_TextChanged(object sender, EventArgs e)
    {

    }

    protected void cfwz_TextChanged(object sender, EventArgs e)
    {

    }
}