﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Model;
using System.Text;
using BL;

public partial class Login : System.Web.UI.Page
{
    Administrator administrator = new Administrator();
    Student student = new Student();
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        
        if (DropDownList1.SelectedItem.Value == "管理员")
        {
            AdministratorBL administratorBL = new AdministratorBL();
            string inputPassword =this. pwd.Text.Trim();
            string password = administratorBL.GetPasswordByAdministratorID(this.num.Text.Trim());

            if (inputPassword == password)
            {
                Session[administrator.AdministratorID] = this.num.Text.Trim();
                Response.Redirect("Default.aspx");
            }
            else
                Response.Write("用户名或密码错误！");
        }
        else if (DropDownList1.SelectedItem.Value == "学生")
        {
            StudentBL studentBL = new StudentBL();
            string inputPassward = this.pwd.Text.Trim();
            string passward = studentBL.GetPasswardByStudnetID(this.num.Text.Trim());
            if (inputPassward == passward)
            {
                Session[student.StudentID] = this.num.Text.Trim();
                Response.Redirect("Default1.aspx");
            }
            else
            {
                Response.Write("用户名或密码错误!");
            }
        }
        else
        {
            ClientScript.RegisterStartupScript(this.GetType(),
                    "", "<script>alert('暂时没有此用户类型！');</script>"); return;
        }
}
    protected void Button2_Click(object sender, EventArgs e)
    {
        this.num.Text = "";
        this.pwd.Text = "";
    }

    protected void num_TextChanged(object sender, EventArgs e)
    {

    }

    protected void pwd_TextChanged(object sender, EventArgs e)
    {

    }

    protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
}