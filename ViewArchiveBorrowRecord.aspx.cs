﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BL;
using Model;

public partial class ViewArchiveBorrowRecord : System.Web.UI.Page
{
    ArchiveBorrowRecordBL archiveBorrowRecordBL = new ArchiveBorrowRecordBL();
    ArchiveBorrowRecord archiveBorrowRecord = new ArchiveBorrowRecord();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindArchiveBorrowRecord();
        }
    }
    //绑定数据
    protected void BindArchiveBorrowRecord()
    {
        this.GridView1.DataSource = archiveBorrowRecordBL.GetArchiveBorrowRecord();
        this.GridView1.DataBind();
    }

    //执行数据行绑定事件
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Attributes.Add("onmouseover", "this.style.backgroundcolor = currentcolor");
            ImageButton imgbtn = (ImageButton)e.Row.FindControl("imgbtnDelete");
            imgbtn.Attributes.Add("onclick", "return confirm('确认删除吗？');");
        }
    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        string cmd = e.CommandName;
        string  id = Convert.ToString(e.CommandArgument);
        if (cmd == "De")
        {
            archiveBorrowRecordBL.DeleteArchiveBorrowRecordByBorrowID(id);
        }
        else if(cmd == "Ed"){
            Page.Server.Transfer("EditArchiveBorrowRecord.aspx? BorrowID=" + id.ToString());
        }
        BindArchiveBorrowRecord();
    }
    
    //查询
    protected void Button1_Click(object sender, EventArgs e)
    {
        string inputStudentID = this.xh.Text.Trim();
        //archiveBorrowRecord.FileNumber = Convert.ToString(this.xh.Text.Trim());
        archiveBorrowRecordBL.GetArchiveBorrowRecordByStudentID(inputStudentID);
        BindArchiveBorrowRecord();
    }
    //分页
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        BindArchiveBorrowRecord();
    }
}