﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ViewActivityAwardInformation.aspx.cs" Inherits="ViewActivityAwardInformation" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>海南大学学生档案管理系统</title>
    <style type="text/css">
        .mainbody
        {
            height:100%;
            width: 100%;
            background-color:#E6E6FA;
        }
        #bodyDiv
        {
            height:100%;
            width: 100%;
        }
        .head
        {
            width: 100%;
            height: 35px;
        }
        
        .top
        {
            height: 41px;
            width: 100%;
        }
        #table1
        {
            height: 72px;
            width: 517px;
            margin-right: 614px;
            margin-bottom: 9px;
            margin-left:490px;
        }
        .logo
        {
            height: 135px;
            width:100%;
           background-color:#B2DFEE;
        }
              .style4
        {
            width: 593px;
        }
        .style6
        {
            width: 500px;
        }
              .style7
        {
            width: 118px;
        }
        .style8
        {
            width: 204px;
        }
              .style9
        {
            width: 317px;
        }
              </style>
</head>
<body class="mainbody">
    <form id="form1" runat="server"  method="post" action="ViewActivityAwardInformation.aspx">
    <div id="bodyDiv">
        <div class="logo">
            <h2 ><img src="picture/logo_school.png" style="height: 70px" />
                <img src="picture/logo2.png" 
                    style="margin-left: 442px; height: 72px; width: 372px;" />
                      
            </h2>
             <span id="Label3" style="font-size:x-large; color:Black">&nbsp;欢迎您：</span>    
             <asp:Label ID="glyxm" runat="server" Text="Label" Font-Size="Large" BackColor="#B2DFEE"></asp:Label>&nbsp; 管理员                     
        </div>
       <hr style="height: 7px; width:100%; background-color:#B2DFEE" />
           <table>
              <tr>
                <td class="style8" ><a class ='top_link ' href='Default.aspx' style="font-size:x-large; color:Olive">返回首页</a></td>
                <td class="style6" ><span id="Label4" style="font-size:x-large; color:Olive">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 当前位置： </span></td>
                <td class="style4" ><span id="Label5"  style="font-size:x-large; color:Olive">&nbsp;管理学生活动奖励</span></td>
                <td class="style7">&nbsp;&nbsp; <a id="likTc" href="Login.aspx"">退出</a></td>
            </tr>
          </table>
            <hr style="height: 2px; width:100%; background-color:#B2DFEE" />
        <br />
        <br /><br />
      <table style="margin-left: 267px; width: 698px;">
      <tr>
        <td>学号：<asp:TextBox ID="xh" runat="server"></asp:TextBox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button ID="Button1" runat="server" Text="查询" Height="26px"  BackColor="#B2DFEE"
            onclick="Button1_Click" Width="88px" /></td>
      </tr>
      <tr><td></td></tr>
         <tr><td>
         <asp:GridView ID="GridView1" runat="server" AllowPaging="True"
            AutoGenerateColumns="False"  DataKeyNames="AwardID" CellSpacing="2" CellPadding="3" Font-Size="Large"
            EmptyDataText="没有可显示的数据记录。" BackColor="White" BorderColor="#3366CC" 
            BorderStyle="None" Width="691px" OnRowDataBound="GridView1_RowDataBound" OnRowCommand="GridView1_RowCommand" OnPageIndexChanging="GridView1_PageIndexChanging">
           <Columns>
            <asp:BoundField DataField="IDAward" HeaderText="AwardID" ReadOnly="True" ItemStyle-HorizontalAlign="Center" ItemStyle-CssClass="style7" SortExpression="AwardID" />
            <asp:BoundField DataField="StudentID" HeaderText="StudentID" ReadOnly="True" ItemStyle-HorizontalAlign="Center" ItemStyle-CssClass="style7" SortExpression="StudentID" />
            <asp:BoundField DataField="AwardDiscription" HeaderText="AwardDiscription" ItemStyle-HorizontalAlign="Center" ItemStyle-CssClass="style7" SortExpression="AwardDiscription" />
            <asp:BoundField DataField="AwardType" HeaderText="AwardType" ItemStyle-HorizontalAlign="Center" ItemStyle-CssClass="style7" SortExpression="AwardType" />
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:ImageButton ID="imgbtnEdit" runat="server" AlternateText="编辑" CommandArgument=<%#Eval("AwardID") %> CommandName="Ed" />
                </ItemTemplate>
            </asp:TemplateField>
               <asp:TemplateField>
                <ItemTemplate>
                    <asp:ImageButton ID="imgbtnDelete" runat="server" AlternateText="删除" CommandArgument=<%#Eval("AwardID") %> CommandName="De" />
                </ItemTemplate>
            </asp:TemplateField>
           </Columns>
             <FooterStyle BackColor="#99CCCC" ForeColor="#003399" />
             <HeaderStyle BackColor="#003399" Font-Bold="True" ForeColor="#CCCCFF" />
             <PagerStyle BackColor="#99CCCC" ForeColor="#003399" HorizontalAlign="Left" />
             <RowStyle BackColor="White" ForeColor="#003399" />
             <SelectedRowStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
             <SortedAscendingCellStyle BackColor="#EDF6F6" />
             <SortedAscendingHeaderStyle BackColor="#0D4AC4" />
             <SortedDescendingCellStyle BackColor="#D6DFDF" />
             <SortedDescendingHeaderStyle BackColor="#002876" />
           </asp:GridView>

           </td></tr>
           </table>
    </div>
    </form>
</body>
</html>
