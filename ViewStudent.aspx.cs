﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DB;
using BL;
using Model;

public partial class ViewStudent : System.Web.UI.Page
{
    StudentBL studentBL = new StudentBL();
    Student student = new Student();
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindStudent();
        }
    }

    //绑定数据
    public void BindStudent()
    {
        this.GridView1.DataSource = studentBL.GetStudent();
        this.GridView1.DataBind();
    }
    //查询
    protected void Button1_Click(object sender, EventArgs e)
    {
        string inputStudentID = this.xh.Text.Trim();
        //student.StudentID = Convert.ToString(this.xh.Text.Trim());
        studentBL.GetStudentByStudentID(inputStudentID);
        BindStudent();
    }
    //执行grid view数据行绑定时间
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Attributes.Add("onmouseover", "this.style.backgroundcolor=currentcolor");
            ImageButton imgbtn = (ImageButton)e.Row.FindControl("imgbtnDelete");
            imgbtn.Attributes.Add("onclick", "return confirm('确认删除吗？');");
        }
    }
    //执行gridview 数据行按钮事件
    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        string cmd = e.CommandName;
        string studentID = Convert.ToString(e.CommandArgument);
        if (cmd == "De")
        {
           
        }
        else if (cmd == "Ed")
        {
            Page.Server.Transfer("EditStudent.aspx? StudentID=" + Convert.ToString(studentID));
        }
        
        BindStudent();
    }
    //执行分页事件 
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        BindStudent();
    }
}