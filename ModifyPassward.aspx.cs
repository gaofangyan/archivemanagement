﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using Model;
using BL;

public partial class ModifyPassward : System.Web.UI.Page
{
    Administrator administrator = new Administrator();

    protected void Page_Load(object sender, EventArgs e)
    {
        string AdministratorID = Request.QueryString["administrator"];
        if (!IsPostBack)
        {
            if (AdministratorID != null)
            {
                MembershipUser mu = Membership.GetUser(AdministratorID);
                
                
            }
        }
       
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        AdministratorBL administratorBL=new AdministratorBL();
        string inputPassward = this.txtOldPwd.Text.Trim();
        string passward = administratorBL.GetPasswordByAdministratorID(this.glybh.Text.Trim());
        if (inputPassward == passward)
        {
            administrator.AdministratorID = Convert.ToString(this.glybh.Text.Trim());
            administrator.Password = Convert.ToString(this.txtOldPwd.Text.Trim());
            administrator.Password = Convert.ToString(this.txtNewPwd.Text.Trim());
            administrator.Password = Convert.ToString(this.txtNewPwdAgain.Text.Trim());

            int count;
            count = administratorBL.ModifyPassword(administrator);
            if (count == 1)
            {
                ClientScript.RegisterStartupScript(this.GetType(),
                    "", "<script>alert('修改成功！');</script>"); return;
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(),
                    "", "<script>alert('修改失败！');</script>"); return;
            }
        }
        else
        {
            ClientScript.RegisterStartupScript(this.GetType(),
                    "", "<script>alert('原密码错误！');</script>"); return;
        }
        
        
    }

    protected void Button2_Click(object sender, EventArgs e)
    {
        this.glybh.Text = "";
        this.txtOldPwd.Text = "";
        this.txtNewPwd.Text = "";
        this.txtNewPwdAgain.Text = "";
    }

    protected void glybh_TextChanged1(object sender, EventArgs e)
    {

    }
}