﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>海南大学学生档案管理系统</title>
    <style type="text/css">
        .mainbody
        {
            height:100%;
            width: 100%;
            background-color:#E6E6FA;
        }
        #bodyDiv
        {
            height:548px;
            width: 100%;
        }
        .head
        {
            width: 100%;
            height: 35px;
        }
        
        .top
        {
            height: 41px;
            width: 100%;
        }
        #table1
        {
            height: 72px;
            width: 517px;
            margin-right: 614px;
            margin-bottom: 9px;
            margin-left:490px;
        }
        .logo
        {
            height: 135px;
            width:100%;
           background-color:#B2DFEE;
        }
              .style4
        {
            width: 593px;
        }
        .style6
        {
            width: 500px;
        }
              .style8
        {
            width: 204px;
        }
              .style9
        {
            width: 181px;
        }
              </style>
</head>
<body class="mainbody">
    <form id="form1" runat="server"  method="post" action="Login.aspx">
    <div id="bodyDiv">
        <div class="logo">
            <h2 ><img src="picture/logo_school.png"  style="height: 70px" />
                <img src="picture/logo2.png" class="logo"
                    style="margin-left: 442px; height: 72px; width: 372px" />
            </h2>
                   
                                  
        </div>
       <hr style="height: 7px; width:100%; background-color:#B2DFEE" />
           
            <hr style="height: 2px; width:100%; background-color:#B2DFEE" />
        <br />
        <br /><br />
       <div id="footer">
            
            <br />
            <table border="0" cellspacing="0" cellpadding="0" 
                style=" background-color:#aabbcc; margin-left: 444px; width: 416px; height: 292px;">
                 <tr>
                     <td class="style9" ></td>
                     <td class="style2"  ><h2 align="left" style="font-family:Arial TUR">用户登录</h2></td>
                 </tr>

                 <tr>
                     <td align="center" class="style9" >用户名：</td>
                     <td align="left" class="style3">
                         <asp:TextBox ID="num" runat="server" 
                        ontextchanged="num_TextChanged" Height="21px" BackColor="ButtonFace"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="num" ErrorMessage="用户名不能为空!" ForeColor="#bbeeff">
                        </asp:RequiredFieldValidator></td>
                 </tr>
                 <tr><td></td></tr>
                 <tr><td></td></tr>
                 <tr>
                     <td align="center" class="style9">密码：</td>
                     <td align="left" class="style8">
                         <asp:TextBox ID="pwd" TextMode="Password" runat="server" 
                         ontextchanged="pwd_TextChanged" Height="21px" BackColor="ButtonFace"></asp:TextBox>
                       <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="num" ErrorMessage="密码不能为空!" ForeColor="#bbeeff">
                       </asp:RequiredFieldValidator></td>
                 </tr>
                 <tr><td></td></tr>
                 <tr><td></td></tr>
                 <tr>
                     <td align="center" class="style9">类型：</td>
                     <td class="style6" >
                         <asp:DropDownList ID="DropDownList1" runat="server"  BackColor="ButtonFace"
                                   onselectedindexchanged="DropDownList1_SelectedIndexChanged" Height="27px" 
                                   Width="151px"><asp:ListItem>管理员</asp:ListItem>
                                              <asp:ListItem>学生</asp:ListItem>
                                              <asp:ListItem>教师</asp:ListItem></asp:DropDownList></td>                  
                 </tr>
                 <tr>
                     <td class="style9"  ></td>
                     <td class="style4"></td>
                 </tr>
                 <tr><td></td></tr>
                 <tr><td></td></tr>
                 <tr>
                     <td align="right" height="50px" class="style9">
                             &nbsp;&nbsp;&nbsp;
                             <asp:Button ID="Button1" runat="server" Text="登录"
                                  Width="68px" Height="30px" BorderStyle="Solid" BackColor="SkyBlue" 
                                 onclick="Button1_Click" />
                          </td>
                          <td align="center">
                             <asp:Button ID="Button2" runat="server" Text="重置" style="margin-left: 84px" 
                                  Width="72px" onclick="Button2_Click" Height="30px" BorderStyle="Solid" 
                                  BackColor="SkyBlue" />
                          </td>
                 </tr>
            </table>
               
       </div>
       
   </div>

    </form>

</body>
</html>

