﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AddActivityWardInformation.aspx.cs" Inherits="AddActivityWardInformation" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>海南大学学生档案管理系统</title>
    <style type="text/css">
        .mainbody
        {
            height:100%;
            width: 100%;
            background-color:#E6E6FA;
        }
        #bodyDiv
        {
            height:100%;
            width: 100%;
        }
        .head
        {
            width: 100%;
            height: 35px;
        }
        
        .top
        {
            height: 41px;
            width: 100%;
        }
        #table1
        {
            height: 72px;
            width: 517px;
            margin-right: 614px;
            margin-bottom: 9px;
            margin-left:395px;
        }
        .logo
        {
            height: 135px;
            width:100%;
           background-color:#B2DFEE;
        }
              .style4
        {
            width: 593px;
        }
        .style6
        {
            width: 500px;
        }
              .style7
        {
            width: 118px;
        }
        .style8
        {
            width: 204px;
        }
              </style>
</head>
<body class="mainbody">
    <form id="form1" runat="server"  method="post" action="AddActivityWardInformation.aspx">
    <div id="bodyDiv">
        <div class="logo">
            <h2 ><img src="picture/logo_school.png" style="height: 70px" />
                <img src="picture/logo2.png" 
                    style="margin-left: 442px; height: 72px; width: 372px;" />
            </h2>
            <span id="Label3" style="font-size:x-large; color:Black">&nbsp;欢迎您：</span>       
            <asp:Label ID="glyxm" runat="server" Text="Label" Font-Size="Large" BackColor="#B2DFEE"></asp:Label>&nbsp; 管理员                      
        </div>
       <hr style="height: 7px; width:100%; background-color:#B2DFEE" />
           <table>
              <tr>
                <td class="style8" ><a class ='top_link ' href='Default.aspx' style="font-size:x-large; color:Olive">返回首页</a></td>
                <td class="style6" ><span id="Label4" style="font-size:x-large; color:Olive">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 当前位置： </span></td>
                <td class="style4" ><span id="Label5"  style="font-size:x-large; color:Olive">&nbsp;添加学生活动奖励</span></td>
                <td class="style7">&nbsp;&nbsp; <a id="likTc" href="Login.aspx"">退出</a></td>
            </tr>
          </table>
            <hr style="height: 2px; width:100%; background-color:#B2DFEE" />
        <br />
        <br /><br />
 
            <table border="2px" 
            style="border-width: 5px; width: 658px; height: 221px; margin-left: 327px;">
                <tr>
                   <td class="style3"><div style="width: 110px">奖励编号:</div></td>
                   <td><div><asp:TextBox ID="jlbh" runat="server" Width="168px" Height="25px" ></asp:TextBox></div></td>
                </tr>
                <tr>
                   <td class="style3"><div style="width: 110px">学号:</div></td>
                   <td><div><asp:TextBox ID="xh" runat="server" Width="168px" Height="25px" ></asp:TextBox></div></td>
                </tr>
                <tr>
                   <td class="style3"><div>奖励描述:</div></td>
                   <td><div><asp:TextBox ID="jlms" runat="server" TextMode="MultiLine" Width="478px" 
                           Height="66px"></asp:TextBox></div></td>
                </tr>
                <tr>
                   <td class="style3"><div>奖励类别:</div></td>
                   <td><div><asp:TextBox ID="jllb" runat="server" Width="170px" Height="24px"></asp:TextBox></div></td>
                </tr>
            </table>
            <table id="table1" border="0">
             <tr>
              <td align="center" height="50px" class="style1">
                  &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;
               <asp:Button ID="Button1" runat="server" Text="添加" Font-Size="Large" 
                ForeColor="Blue" onclick="Button1_Click" /></td>
              <td class="style2" >
               &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
               <asp:Button ID="Button2" runat="server" Text="取消" Font-Size="Large" 
                ForeColor="Blue" onclick="Button2_Click" style="margin-left: 44px"  /></td>
                </tr>
            </table>

    </div>
</form>
</body>
</html>

