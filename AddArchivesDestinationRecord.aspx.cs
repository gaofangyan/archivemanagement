﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using Model;
using BL;

public partial class AddArchivesDestinationRecord : System.Web.UI.Page
{
    ArchivesDestinationRecord archivesDestinationRecord = new ArchivesDestinationRecord();
    ArchiveInformation archiveInformation = new ArchiveInformation();
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Button1_Click(object sender, EventArgs e)
    {
       ArchivesDestinationRecordBL archivesDestinationRecordBL = new ArchivesDestinationRecordBL();
         /*string  inputfileNumber =this.dabh.Text.Trim();
        string fileNumber = archivesDestinationRecordBL.GetFileNumber(archiveInformation.StudentID);
        if (fileNumber==inputfileNumber)
        {
            ClientScript.RegisterStartupScript(this.GetType(),
                    "", "<script>alert('此档案已存在！');</script>"); return;
        }
        else
        {
        */
            //archivesDestinationRecord.ID = Convert.ToInt32(this.qxID.Text.Trim());
            archivesDestinationRecord.StudentID = this.xh.Text.Trim();
            archivesDestinationRecord.StudentName = this.xsxm.Text.Trim();
            archivesDestinationRecord.ReceivingUnit = this.jsdw.Text.Trim();
            archivesDestinationRecord.ReceivingTime = Convert.ToDateTime(this.jssj.Text.Trim());
            archivesDestinationRecord.ReceivingUnitPhone = this.jsdwdh.Text.Trim();
            archivesDestinationRecord.UnitAddress = this.dwzd.Text.Trim();

            int count = archivesDestinationRecordBL.AddArchivesDestinationRecord(archivesDestinationRecord);
            if (count==1)
            {
                Page.Server.Transfer("ViewArchivesDestinationRecord.aspx");
            }
            else
                ClientScript.RegisterStartupScript(this.GetType(),
                    "", "<script>alert('添加失败！');</script>"); return;
        
    }

    protected void Button2_Click(object sender, EventArgs e)
    {
        this.Dispose();
        Response.Redirect("Default.aspx");
    }
}