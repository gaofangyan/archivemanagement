﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DB;
using Model;
using BL;

public partial class AddStudent : System.Web.UI.Page
{
    Student student = new Student();
    StudentBL studentBL = new StudentBL();
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        string studentID = this.xsxh.Text.Trim();
        string inputStudentID = studentBL.GetStudentIDByStudentName(this.xsxm.Text.Trim());
        if (inputStudentID == studentID)
        {
            ClientScript.RegisterStartupScript(this.GetType(),
                    "", "<script>alert('此学生已存在！');</script>"); return;
        }
        else
        {
            student.StudentID = Convert.ToString(this.xsxh.Text.Trim());
            student.StudentName = Convert.ToString(this.xsxm.Text.Trim());
            student.Passward = Convert.ToString(this.xsmm.Text.Trim());
            student.Sex = Convert.ToString(this.xsxb.Text.Trim());
            student.Profession = Convert.ToString(this.xszy.Text.Trim());
            student.PoliticalStatus = Convert.ToString(this.xszzmm.Text.Trim());
            student.Nation = Convert.ToString(this.mz.Text.Trim());
            student.Birstday = Convert.ToDateTime(this.sr.Text.Trim());
            student.EducationExperience = Convert.ToString(this.xsjyjl.Text.Trim());
            int count = 0;
            count = studentBL.AddStudent(student);
            if (count == 1)
            {
                Page.Server.Transfer("ViewStudent.aspx");
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(),
                    "", "<script>alert('添加失败！');</script>"); return;
            }
        }
    }

    protected void Button2_Click(object sender, EventArgs e)
    {

    }
}