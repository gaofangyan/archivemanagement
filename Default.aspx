﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>海南大学学生档案管理系统</title>
    <style type="text/css">
        .mainbody
        {
            height:838px;
            width: 100%;
            background-color:#E6E6FA;
        }
        #bodyDiv
        {
            height:845px;
            width: 100%;
        }
        .head
        {
            width: 100%;
            height: 35px;
        }
        .top
        {
            height: 41px;
            width: 100%;
        }
        #table1
        {
            height: 72px;
            width: 517px;
            margin-right: 614px;
            margin-bottom: 9px;
            margin-left:490px;
        }
        .logo
        {
            height: 135px;
            width:100%;
           background-color:#B2DFEE;
         }
              .style4
        {
            width: 593px;
        }
        .style6
        {
            width: 500px;
         }
        ul
        {
          list-style-type :none;
          
         }

   </style>
</head>
<body class="mainbody">
    <form id="form1" runat="server"  method="post" action="Default.aspx">
    <div id="bodyDiv">
        <div class="logo">
            <h2 ><img src="picture/logo_school.png" style="height: 70px" />
                <img src="picture/logo2.png" 
                    style="margin-left: 442px; height: 72px; width: 372px" />
            </h2>
            <span id="Label3" style="font-size:x-large; color:Black">&nbsp;欢迎您：</span>       
             <asp:Label ID="glyxm" runat="server" Text="Label" Font-Size="Large" BackColor="#B2DFEE"></asp:Label>&nbsp; 管理员                 
        </div>
       <hr style="height: 7px; width:100%; background-color:#B2DFEE" />
           <table>
              <tr>
                
                <td class="style6" ><span id="Label4" style="font-size:x-large; color:Olive">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;当前位置： </span></td>
                <td class="style4" ><span id="Label5"  style="font-size:x-large; color:Olive">&nbsp;管理员首页</span></td>
                <td class="style7">&nbsp;&nbsp; <a id="likTc" href="Login.aspx"">退出</a></td>
            </tr>
          </table>
            <hr style="height: 2px; width:100%; background-color:#B2DFEE" />
        <br />
 
        <div  style="border-style: none; border-color: inherit; border-width: 2px; margin-right: 822px; background-color:#B2DFEE; height: 393px; width: 301px;">
           <ul class="nav nav-tabs" >
                <li class="active"><a  href='Default.aspx' style="font-size:x-large; color:Olive">返回首页</a></li>
                
                <li class="dropdown"><a href="#a"  class="dropdown-toggle" ><span class="caret" style="font-size:x-large; color:Olive">信息维护</span></a>
                   <ul class="dropdown-menu">
                       <li class="dropdown"><a href="GetCurrentAdministrator.aspx" target='zhuti' onclick="GetMc"('个人信息');" style="color:Gray">个人信息</a></li>
                       <li class="dropdown"><a href="ModifyPassward.aspx" target='zhuti' onclick="GetMc('密码修改')" style="color:Gray">密码修改</a></li>
                   </ul>
                </li>
                
                
                <li class="dropdown"><a href='#a' class='dropdown-toggle'><span class="caret" style="font-size:x-large; color:Olive">用户管理</span></a>
                    <ul class="dropdown-menu">
                         <li><a href="AddStudent.aspx" target="zhuti" onclick="GetMc('添加学生信息')" style="color:gray">添加学生信息</a></li>
                        <li><a href="ViewStudent.aspx" target="zhuti" onclick="GetMc('管理学生信息')" style="color:gray">管理学生信息</a></li>

                    </ul>
                </li>
                <li class="dropdown"><a href='#a' class='dropdown-toggle'><span class='down' style="font-size:x-large; color:Olive">档案管理</span></a>
                    <ul class="dropdown-menu">
                         <li><a href="AddArchiveInformation.aspx" target="zhuti" onclick="GetMc('添加档案信息')" style="color:Gray">添加档案信息</a></li>
                         <li><a href="AddArchiveBorrowRecord.aspx" target='zhuti' onclick="GetMc('添加档案借阅记录')" style="color:Gray">添加档案借阅记录</a></li>
                         <li><a href="AddArchivesDestinationRecord.aspx" target='zhuti' onclick="GetMc('添加档案去向记录')" style="color:Gray">添加档案去向记录</a></li>
                         <li><a href="ViewArchiveInformation.aspx" target='zhuti' onclick="GetMc('管理档案信息')" style="color:Gray">管理档案信息</a></li>
                         <li><a href="ViewArchivesDestinationRecord.aspx" target='zhuti' onclick="GetMc('管理档案去向')" style="color:Gray">管理档案去向记录</a></li>
                         <li><a href="ViewArchiveBorrowRecord.aspx" target='zhuti' onclick="GetMc('管理档案借阅')" style="color:Gray">管理档案借阅记录</a></li>
                    </ul>
                </li>
                <li class="dropdown"><a href='#a' class='dropdown-toggle'><span class='down' style="font-size:x-large; color:Olive">活动奖励</span></a>
                    <ul class="dropdown-menu">
                         <li><a href="AddActivityWardInformation.aspx" target="zhuti" onclick="GetMc('添加活动奖励')" style="color:Gray">添加活动奖励</a></li>
                         <li><a href="ViewActivityAwardInformation.aspx" target='zhuti' onclick="GetMc('管理学生活动奖励')" style="color:Gray">管理学生活动奖励</a></li>
                    </ul>
                </li>
                
            </ul>
            
     </div>
  
    </div>
    </form>
</body>
</html>
