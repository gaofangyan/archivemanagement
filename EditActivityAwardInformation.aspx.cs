﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using Model;
using BL;

public partial class EditActivityAwardInformation : System.Web.UI.Page
{
    ActivityAwardInformation activityAwardInformation = new ActivityAwardInformation();

    protected void Page_Load(object sender, EventArgs e)
    {
        /*string archiveID = Request.QueryString["archive"];
        if (!IsPostBack)
        {
            if (archiveID != null)
            {
                MembershipUser mu = Membership.GetUser(archiveID);
                
                
            }
        }
        */
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        ActivityAwardInformationBL activityAwardInformationBL = new ActivityAwardInformationBL();
        activityAwardInformation.AwardDiscription = Convert.ToString(this.jlms.Text.Trim());
        activityAwardInformation.AwardType = Convert.ToString(this.jllb.Text.Trim());
        int count;
        count = activityAwardInformationBL.ModifyActivityAwardInformation(activityAwardInformation);
        if (count == 1)
        {
            Page.Server.Transfer("ViewActivityAwardInformation.aspx");
        }
        else
            ClientScript.RegisterStartupScript(this.GetType(),
                  "", "<script>alert('修改失败！');</script>"); return;
    }

    protected void Button2_Click(object sender, EventArgs e)
    {
        this.Dispose();
        Response.Redirect("ViewActivityAwardInformation.aspx");
    }

    protected void dabh_TextChanged(object sender, EventArgs e)
    {

    }
}