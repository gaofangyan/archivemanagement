﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Model;
using BL;

public partial class AddArchiveInformation : System.Web.UI.Page
{
    


    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void dabh_TextChanged(object sender, EventArgs e)
    {

    }

    protected void xh_TextChanged(object sender, EventArgs e)
    {

    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        ArchiveInformationBL archiveInformationBL = new ArchiveInformationBL();
        string  inputfileNumber = this.dabh.Text.Trim();
        string fileNumber = archiveInformationBL.GetFileNumberByStudentID(this.xh.Text.Trim());
        if (inputfileNumber == fileNumber)
        {
            ClientScript.RegisterStartupScript(this.GetType(),
                    "", "<script>alert('此档案已存在！');</script>"); return;
        }
        else
        {
            ArchiveInformation archiveInformation = new ArchiveInformation();
            archiveInformation.FileNumber = this.dabh.Text.Trim();
            archiveInformation.Student = new StudentBL().GetStudentByStudentID(xh.Text.Trim())[0];
         //   archiveInformation.ActivityAwardInformation[0].AwardID = Convert.ToString(this.jlbh.Text.Trim());
           // archiveInformation.ArchiveBorrowRecord[0].BorrowID = Convert.ToString(this.jybh.Text.Trim());
            archiveInformation.Address = this.cfwz.Text.Trim();
            archiveInformation.TransferTime = Convert.ToDateTime(this.drsj.Text.Trim());

            int count = archiveInformationBL.AddArchiveInformation(archiveInformation);
            if (count == 1)
            {
                Page.Server.Transfer("ViewArchiveInformation.aspx");
            }
            else
                ClientScript.RegisterStartupScript(this.GetType(),
                   "", "<script>alert('添加失败！');</script>"); return;
        }
    }

    protected void Button2_Click(object sender, EventArgs e)
    {

    }

    protected void cfwz_TextChanged(object sender, EventArgs e)
    {

    }
}