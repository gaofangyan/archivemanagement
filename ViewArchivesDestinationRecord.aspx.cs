﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BL;
using Model;

public partial class ViewArchivesDestinationRecord : System.Web.UI.Page
{
    ArchivesDestinationRecordBL archivesDestinationRecordBL = new ArchivesDestinationRecordBL();
    ArchivesDestinationRecord archivesDestinationRecord = new ArchivesDestinationRecord();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindArchivesDestinationRecord();
        }
    }

    //绑定档案去向记录信息
    protected void BindArchivesDestinationRecord()
    {
        this.GridView1.DataSource = archivesDestinationRecordBL.GetArchivesDestinationRecord();
        this.GridView1.DataBind();
    }

    //执行grid view数据行绑定事件
    protected void GridView1RowDataBound(object sender,GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Attributes.Add("onmouseover", "this.style.backgroundcolor=currentcolor");
            ImageButton imgbtn = (ImageButton)e.Row.FindControl("imgbtnDelete");
            imgbtn.Attributes.Add("onclick", "return confirm('确认删除吗？');");
        }
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        string inputStudentID = this.xh.Text.Trim();
        archivesDestinationRecord.StudentID = Convert.ToString(this.xh.Text.Trim());
        archivesDestinationRecordBL.GetArchivesDestinationRecordByStudentID(inputStudentID);
        BindArchivesDestinationRecord();

    }

    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    //执行grid view 数据行按钮事件
    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        string cmd = e.CommandName;
        string  id = Convert.ToString(e.CommandArgument); //获取命令参数
        if (cmd == "De")
        {
            archivesDestinationRecordBL.DeleteArchivesDestinationRecord(id);
        }
        else if (cmd == "Ed")
        {
            Page.Server.Transfer("EditArchivesDestinationRecord.aspx? DestinationID=" + id.ToString());
        }
        BindArchivesDestinationRecord();
    }

    //执行分页事件
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        BindArchivesDestinationRecord();
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {

    }
}