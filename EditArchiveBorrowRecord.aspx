﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EditArchiveBorrowRecord.aspx.cs" Inherits="EditArchiveBorrowRecord" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>海南大学学生档案管理系统</title>
    <style type="text/css">
        .mainbody
        {
            height:100%;
            width: 100%;
            background-color:#E6E6FA;
        }
        #bodyDiv
        {
            height:100%;
            width: 100%;
        }
        .head
        {
            width: 100%;
            height: 35px;
        }
        
        .top
        {
            height: 41px;
            width: 100%;
        }
        #table1
        {
            height: 72px;
            width: 517px;
            margin-right: 614px;
            margin-bottom: 9px;
            margin-left:354px;
        }
        .logo
        {
            height: 135px;
            width:100%;
           background-color:#B2DFEE;
        }
              .style4
        {
            width: 593px;
        }
        .style6
        {
            width: 500px;
        }
              .style7
        {
            width: 118px;
        }
        .style8
        {
            width: 204px;
        }
              </style>
</head>
<body class="mainbody">
    <form id="form1" runat="server"  method="post" action="EditArchiveBorrowRecord.aspx">
    <div id="bodyDiv">
        <div class="logo">
            <h2 ><img src="picture/logo_school.png" style="height: 70px" />
                <img src="picture/logo2.png" 
                    style="margin-left: 442px; height: 72px; width: 372px;" />
            </h2>
            <span id="Label3" style="font-size:x-large; color:Black">&nbsp;欢迎您：</span>       
            <asp:Label ID="glyxm" runat="server" Text="Label" Font-Size="Large" BackColor="#B2DFEE"></asp:Label>&nbsp; 管理员                      
        </div>
       <hr style="height: 7px; width:100%; background-color:#B2DFEE" />
           <table>
              <tr>
                <td class="style8" ><a class ='top_link ' href='Default.aspx' style="font-size:x-large; color:Olive">返回首页</a></td>
                <td class="style6" ><span id="Label4" style="font-size:x-large; color:Olive">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 当前位置： </span></td>
                <td class="style4" ><span id="Label5"  style="font-size:x-large; color:Olive">&nbsp;修改档案借阅记录</span></td>
                <td class="style7">&nbsp;&nbsp; <a id="likTc" href="Login.aspx"">退出</a></td>
            </tr>
          </table>
            <hr style="height: 2px; width:100%; background-color:#B2DFEE" />
        <br />
        <br /><br />

        <table border="2px" style="width: 686px; height: 267px; margin-left: 297px">
        <tr>
            
            <td>学号:</td><td><asp:TextBox ID="xh" runat="server" ></asp:TextBox></td>
        </tr>
        <tr>
            <td>借阅人姓名:</td><td><asp:TextBox ID="jyrxm" runat="server" ></asp:TextBox>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 借阅人电话:<asp:TextBox ID="jyrdh" runat="server" ></asp:TextBox></td> 
         </tr>
         <tr>
           <td> 邮箱:</td><td><asp:TextBox ID="jyryx" runat="server" ></asp:TextBox> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 借阅时间:<asp:TextBox ID="jysj" runat="server"></asp:TextBox></td>
         </tr>
         <tr>
            <td>借阅目的:</td><td> 
             <asp:TextBox ID="jymd" runat="server" 
                TextMode="MultiLine" Height="48px" Width="519px"></asp:TextBox></td>
         </tr>
         <tr>
            <td>是否归还:</td><td><asp:TextBox ID="sfgh" runat="server"></asp:TextBox> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 归还日期:<asp:TextBox ID="ghrq" runat="server"></asp:TextBox></td>
          </tr>
          <tr>
            <td>备注:</td>
            <td><asp:TextBox ID="bz" runat="server" TextMode="MultiLine" Height="47px" 
                Width="520px"></asp:TextBox></td>
           </tr>
          </table>
            <table id="table1" border="0">
             <tr>
              <td align="center" height="50px" class="style1">
                  &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;
               <asp:Button ID="Button1" runat="server" Text="更新" Font-Size="Large" 
                ForeColor="Blue" onclick="Button1_Click" /></td>
              <td class="style2" >
               &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
               <asp:Button ID="Button2" runat="server" Text="取消" Font-Size="Large" 
                ForeColor="Blue" onclick="Button2_Click" style="margin-left: 44px"  /></td>
                </tr>
            </table>
</div>
</form>
</body>
</html>

