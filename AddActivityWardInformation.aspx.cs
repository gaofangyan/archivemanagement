﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Model;
using BL;

public partial class AddActivityWardInformation : System.Web.UI.Page
{
    ActivityAwardInformation activityAwardInformation = new ActivityAwardInformation();
    ActivityAwardInformationBL activityAwardInformationBL = new ActivityAwardInformationBL();
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        ActivityAwardInformationBL activityAwardInformationBL = new ActivityAwardInformationBL();
        /*string  inputfileNumber =this.dabh.Text.Trim();
        string  fileNumber = activityAwardInformationBL.GetFileNumber();
        if (inputfileNumber == fileNumber)
        {
            ClientScript.RegisterStartupScript(this.GetType(),
                    "", "<script>alert('此档案已存在！');</script>"); return;
        }
        else
        {
        */
            activityAwardInformation.AwardID = this.jlbh.Text.Trim();
            activityAwardInformation.StudentID = this.xh.Text.Trim();
            activityAwardInformation.AwardDiscription = this.jlms.Text.Trim();
            activityAwardInformation.AwardType = this.jllb.Text.Trim();

            int count = activityAwardInformationBL.AddActivityAwardInformation(activityAwardInformation);
            if (count == 1)
            {
                Page.Server.Transfer("ViewActivityAwardInformation.aspx");
            }
            else
                ClientScript.RegisterStartupScript(this.GetType(),
                    "", "<script>alert('添加失败！');</script>"); return;

    }

    protected void Button2_Click(object sender, EventArgs e)
    {
        this.Dispose();
        Response.Redirect("Default.aspx");
    }
}