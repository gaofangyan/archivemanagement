﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
/// <summary>
///Activity_Award_information 的摘要说明
/// </summary>
/// 
namespace Model
{
    public class ActivityAwardInformation
    {
        private string awardID;
        private string studentID;
        private string awardDiscription;
        private string awardType;

        public ActivityAwardInformation() { }
        /*public ActivityAwardInformation(string studentId, string awardDiscription, string awardType)
        {
            id = 0;
            StudentId = studentId;
            AwardDiscription = awardDiscription;
            AwardType = awardType;
        }*/

       
        public string AwardDiscription
        {
            get
            {
                return awardDiscription;
            }

            set
            {
                awardDiscription = value;
            }
        }

        public string AwardType
        {
            get
            {
                return awardType;
            }

            set
            {
                awardType = value;
            }
        }

        public string  AwardID
        {
            get
            {
                return awardID;
            }

            set
            {
                awardID = value;
            }
        }
        public string StudentID
        {
            get
            {
                return studentID;

            }
            set
            {
                studentID = value;
            }
        }
 
    }

}