﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace Model
{
    public class Student
    {
        private string studentID;
        private string studentName;
        private string passward;
        private string sex;
        private string nation;
        private string politicalStatus;
        private DateTime birstday;
        private string profession;
        private string educationExperience;
        //private ArchiveInformation archiveInformation;
        /*private string iDCard;
        private string telephoneNumber;
        private string domicile;//户籍所在地
        private string eMail;
        private string greda;
        private bool ifGraduated;
        private DateTime graduatedTime;
        private int years;//学制年限
        private string healthyStatus;
        private string familyMember;
        private string educationExperience;
        private string foreignLanguageLevel;
        private string computerSkillLevel;
        */
        public Student() { }
        public Student(string id,string studentName) {
            this.StudentName = studentName;
            this.StudentID = id;
        }

        public string StudentID
        {
            get
            {
                return studentID;
            }
            set
            {
                studentID = value;
            }
        }
        public string StudentName
        {
            get { return studentName; }
            set { studentName = value; }
        }
        public string Passward
        {
            get { return passward; }
            set { passward = value; }
        }
        public string Sex
        {
            get { return sex; }
            set { sex = value; }
        }
        public string Nation
        {
            get { return nation; }
            set { nation = value; }
        }
        public string PoliticalStatus
        {
            get { return politicalStatus; }
            set { politicalStatus = value; }
        }
        public DateTime Birstday
        {
            get { return birstday; }
            set { birstday = value; }
        }
        public string Profession
        {
            get { return  profession; }
            set { profession = value; }
        }
        public string EducationExperience
        {
            get { return educationExperience; }
            set { educationExperience = value; }
        }
        //public ArchiveInformation ArchiveInformation { get => archiveInformation; set => archiveInformation = value; }
        /*public string IDCard { get => iDCard; set => iDCard = value; }
public string TelephoneNumber { get => telephoneNumber; set => telephoneNumber = value; }
public string Domicile { get => domicile; set => domicile = value; }
public string EMail { get => eMail; set => eMail = value; }
public string Greda { get => greda; set => greda = value; }
public bool IfGraduated { get => ifGraduated; set => ifGraduated = value; }
public DateTime GraduatedTime { get => graduatedTime; set => graduatedTime = value; }
public int Years { get => years; set => years = value; }
public string HealthyStatus { get => healthyStatus; set => healthyStatus = value; }
public string FamilyMember { get => familyMember; set => familyMember = value; }
public string EducationExperience { get => educationExperience; set => educationExperience = value; }
public string ForeignLanguageLevel { get => foreignLanguageLevel; set => foreignLanguageLevel = value; }
public string ComputerSkillLevel { get => computerSkillLevel; set => computerSkillLevel = value; }
*/
    }

}
