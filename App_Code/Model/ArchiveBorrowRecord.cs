﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

/// <summary>
///ArchiveBorrow 的摘要说明
/// </summary>
/// 
namespace Model
{
    public class ArchiveBorrowRecord
    {
        private string  borrowID;  //借阅编号
        private string studentID;
        private string borrowerName;
        private string borrowerPhone;
        private string e_mail;
        private DateTime borrowTime;
        private string borrowPurpose;
        private bool ifReturn;
        private DateTime returnTime;
        private string remarks;

        public ArchiveBorrowRecord()
        {
        }
        /*public ArchiveBorrowRecord(string iFileNumber, string iBorrowerName, string iBorrowerPhone, string iE_Mail)
        {
            iD = 0;
            FileNumber = iFileNumber;
            BorrowerName = iBorrowerName;
            BorrowerPhone = iBorrowerPhone;
            EMail = iE_Mail;
        }
        public ArchiveBorrowRecord(DateTime iBorrow_time, string iBorrow_purpose, bool iIfReturn, DateTime iReturn_time, string iRemarks)
        {
            BorrowTime = iBorrow_time;
            BorrowPurpose = iBorrow_purpose;
            IfReturn = iIfReturn;
            ReturnTime = iReturn_time;
            Remarks = iRemarks;
        }
        */

        public string BorrowerName
        {
            get
            {
                return borrowerName;
            }

            set
            {
                borrowerName = value;
            }
        }

        public string BorrowerPhone
        {
            get
            {
                return borrowerPhone;
            }

            set
            {
                borrowerPhone = value;
            }
        }

        public string E_mail
        {
            get
            {
                return e_mail;
            }

            set
            {
                e_mail = value;
            }
        }

        public DateTime BorrowTime
        {
            get
            {
                return borrowTime;
            }

            set
            {
                borrowTime = value;
            }
        }

        public string BorrowPurpose
        {
            get
            {
                return borrowPurpose;
            }

            set
            {
                borrowPurpose = value;
            }
        }

        public bool IfReturn
        {
            get
            {
                return ifReturn;
            }

            set
            {
                ifReturn = value;
            }
        }

        public DateTime ReturnTime
        {
            get
            {
                return returnTime;
            }

            set
            {
                returnTime = value;
            }
        }

        public string Remarks
        {
            get
            {
                return remarks;
            }

            set
            {
                remarks = value;
            }
        }

        public string  BorrowID
        {
            get
            {
                return borrowID;
            }

            set
            {
                borrowID = value;
            }
        }

        public string   StudentID
        {
            get
            {
              return  studentID;
            }
            set
            {
                studentID = value;
            }
        }
    }
}