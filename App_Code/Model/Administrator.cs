﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Administrator 的摘要说明
/// </summary>
namespace Model
{
    public class Administrator
    {
        //private int iD;
        private string  administratorID;
        private string administratorName;
        private string password;
        private string sex;
        private string eMail;

        
        public Administrator()
        {
        }

        public string AdministratorName
        {
            get
            {
                return administratorName;
            }

            set
            {
                administratorName = value;
            }
        }

        public string Password
        {
            get
            {
                return password;
            }

            set
            {
                password = value;
            }
        }

        public string Sex
        {
            get
            {
                return sex;
            }

            set
            {
                sex = value;
            }
        }

        public string EMail
        {
            get
            {
                return eMail;
            }

            set
            {
                eMail = value;
            }
        }

        public string  AdministratorID
        {
            get
            {
                return administratorID;
            }

            set
            {
                administratorID = value;
            }
        }
    }
}