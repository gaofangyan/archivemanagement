﻿using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Text;
using System.Collections.Generic;
using System;
using Model;
/// <summary> 
///archive 的摘要说明
/// </summary>
/// 
namespace Model
{
    public class ArchiveInformation
    {
        //private int id;
        private string fileNumber;
        private Student student;
        private string address;
        private DateTime transferTime;

        IList<ActivityAwardInformation> activityAwardInformation;
        IList<ArchiveBorrowRecord> archiveBorrowRecord = new List<ArchiveBorrowRecord>();

        public Student Student
        {
            get { return student; }
            set { student = value; }
        }

        /// <summary>
        /// 添加新的奖励
        /// </summary>
        /// <param name="award"></param>
        public void AddNewAward(ActivityAwardInformation award)
        {
            ActivityAwardInformation.Add(award);
        }
        public void AddArchiveBorrowRecord(ArchiveBorrowRecord archiveBorrowRecord)
        {
            ArchiveBorrowRecord.Add(archiveBorrowRecord);
        }
        
        public ArchiveInformation()
        {
        }


        /*public ArchiveInformation(string FileNumber, string iStudentId, string iAdrress, string iAdministratorNumber, DateTime iTransferTime)
{
   id = 0;
   this.FileNumber = FileNumber;
   this.StudentId = iStudentId;
   Address = iAdrress;
   AdministratorNumber = iAdministratorNumber;
   TransferTime = iTransferTime;
}
*/
        //ActivityAwardInformation activityAwardInformation = new Model.ActivityAwardInformation();

        public string  FileNumber
        {
            get
            {
                return fileNumber;
            }

            set
            {
                fileNumber = value;
            }
        }

        public string Address
        {
            get
            {
                return address;
            }

            set
            {
                address = value;
            }
        }

        /// <summary>
        /// 调入时间
        /// </summary>
        public DateTime TransferTime
        {
            get
            {
                return transferTime;
            }

            set
            {
                transferTime = value;
            }
        }

        public IList<ActivityAwardInformation> ActivityAwardInformation
        {
            get { return activityAwardInformation; }
            set { activityAwardInformation = value; }
        }
        public IList<ArchiveBorrowRecord> ArchiveBorrowRecord
        {
            get { return  archiveBorrowRecord; }
            set { archiveBorrowRecord = value; }
        }
        

    }

}