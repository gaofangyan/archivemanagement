﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
/// <summary>
///ArchivesDistinationRecord 的摘要说明
/// </summary>
/// 
namespace Model
{
    public class ArchivesDestinationRecord
    {

        private int  destinationID;
        private string studentID;
        private string studentName;
        private string receivingUnit;
        private string receivingUnitPhone;
        private DateTime receivingTime;
        private string unitAddress;


        public ArchivesDestinationRecord()
        {
        }
        /*public ArchivesDestinationRecord(string fileNumber, string receivingUnit, string receivingUnitPhone, DateTime receivingTime, string unitAddress)
        {
            id = 0;
            FileNumber = fileNumber;
            ReceivingUnit = receivingUnit;
            ReceivingUnitPhone = receivingUnitPhone;
            ReceivingTime = receivingTime;
            UnitAddress = unitAddress;

        }
        */

        public string  StudentID
        {
            get
            {
                return studentID;
            }

            set
            {
                studentID = value;
            }
        }
        

        public string ReceivingUnit
        {
            get
            {
                return receivingUnit;
            }

            set
            {
                receivingUnit = value;
            }
        }

        public string ReceivingUnitPhone
        {
            get
            {
                return receivingUnitPhone;
            }

            set
            {
                receivingUnitPhone = value;
            }
        }

        public DateTime ReceivingTime
        {
            get
            {
                return receivingTime;
            }

            set
            {
                receivingTime = value;
            }
        }

        public string UnitAddress
        {
            get
            {
                return unitAddress;
            }

            set
            {
                unitAddress = value;
            }
        }

        public int  DestinationID
        {
            get
            {
                return destinationID;
            }

            set
            {
                destinationID = value;
            }
        }

        public string StudentName
        {
            get
            {
                return studentID;
            }
            set
            {
                studentID = value;
            }
        }
    }
}