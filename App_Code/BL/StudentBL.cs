﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Model;
using DA;

namespace BL
{
    public class StudentBL
    {
        StudentService studentService = new StudentService();

        //根据用户名得到密码
        public string GetPasswardByStudnetID(string studentID)
        {
            string pwd = string.Empty;
            try
            {
                pwd = studentService.GetPasswardByStudnetID(studentID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
            return pwd;
        }

        //根据姓名得到用户名
        public string GetStudentIDByStudentName(string studentName)
        {
            string studentID = string.Empty;
            try
            {
                studentID = studentService.GetStudentIDByStudentName(studentName);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
            return studentID;
        }

        //根据学号得到学生信息
        public IList<Student> GetStudentByStudentID(string studentID)
        {
            try
            {
                return studentService.GetStudentByStudentID(studentID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }


        //得到学生实体
        public Student GetStudent(string studentID)
        {
            try
            {
                return studentService.GetStudent(studentID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        //得到学生信息
        public IList<Student> GetStudent()
        {
            try
            {
                return studentService.GetStudent();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        //修改密码
        public int ModifyPassward(Student student)
        {
            int id = 0;
            try
            {
                id = studentService.ModifyPassward(student);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
            return id;
        }

        //增加学生信息
        public int AddStudent(Student student)
        {
            int id = 0;
            try
            {
                id = studentService.AddStudent(student);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
            return id;
        }

        //编辑学生信息
        public int ModifyStudent(Student student)
        {
            int id = 0;
            try
            {
                id = studentService.ModifyStudent(student);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
            return id;
        }
    }
}
