﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using Model;
using DA;

/// <summary>
/// ArchiveInformation_BL 的摘要说明
/// </summary>
/// 
namespace BL
{
    public class ArchiveInformationBL
    {
        ArchiveInformationService archiveInformationService = new ArchiveInformationService();
        //public ArchiveInformationService archiveInformationService { get; set; }

        public ArchiveInformationBL()
        {
        }

        //通过档案编号获取档案信息实体对象
        public ArchiveInformation GetArchiveInformation(string   fileNumber)
        {

            try
            {
                return archiveInformationService.GetArchiveInformation(fileNumber);
            }
            catch(Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        //通过学号获取档案信息
        public IList<ArchiveInformation> GetArchiveInformationByStudentID(string studentID)
        {
            try
            {
                return archiveInformationService.GetArchiveInformationByStudentID(studentID);
            }
            catch(Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        //通过学号获得档案编号
        public string GetFileNumberByStudentID(string studentID)
        {
            string fileNumber = string.Empty;
            try
            {
                fileNumber = archiveInformationService.GetFileNumberByStudnetID(studentID);
            }
            catch(Exception ex)
            {
                throw new Exception(ex.ToString());
            }
            return fileNumber;
        }

        //通过档案编号获取学号
        public string GetStudnetIDByFileNumber(string  fileNumber)
        {
            string studentID = string.Empty;
            try
            {
                studentID=archiveInformationService.GetFileNumberByStudnetID(fileNumber);
            }
            catch(Exception ex)
            {
                throw new Exception(ex.ToString());
            }
            return studentID;
        }

        //获取档案信息表
        public IList<ArchiveInformation> GetArchiveInformation()
        {
            try
            {
                return archiveInformationService.GetArchiveInformation();
            }
            catch(Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        //删除档案信息
        public void DeleteArchiveInformationByFileNumber(string   fileNumber)
        {
            try
            {
                archiveInformationService.DeleteArchiveInformationByFileNumber(fileNumber);
            }
            catch(Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        //修改档案信息
        public int  ModifyArchiveInformation(ArchiveInformation archiveInformation)
        {
           
            try
            {
               return  archiveInformationService.ModifyArchiveInformation(archiveInformation);
            }
            catch(Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
        //增加档案信息
        public int  AddArchiveInformation(ArchiveInformation archiveInformation)
        {
            try
            {
                return archiveInformationService.AddArchiveInformation(archiveInformation);
            }
            catch(Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
    }
}