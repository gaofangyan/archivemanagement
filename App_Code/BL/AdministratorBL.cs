﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using DA;
using Model;

/// <summary>
/// AdministratorBL 的摘要说明
/// </summary>
/// 
namespace BL
{
    public class AdministratorBL
    {

        AdministratorService administratorService = new AdministratorService();
        public AdministratorBL()
        {

        }

        //管理员编号获取密码业务逻辑
        public string GetPasswordByAdministratorID(string  administratorID)
        {
            string pwd = string.Empty;
            try
            {
                pwd = administratorService.GetPasswordByAdministratorID(administratorID);
            }
            catch(Exception ex)
            {
                throw new Exception(ex.ToString());
            }
            return pwd;
        }

        //修改密码
        public int ModifyPassword(Administrator administrator)
        {
            int id;
            try
            {
                id=administratorService.ModifyPassword(administrator); 
            }
            catch(Exception ex)
            {
                throw new Exception(ex.ToString());
            }
            return id;
        }

        //获取当前信息
        public IList<Administrator> GetAdministrator(string administratorID)
        {
            try
            {
                return administratorService.GetAdministrator(administratorID);

            }
            catch(Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
    }
}