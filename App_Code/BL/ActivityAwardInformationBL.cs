﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using DA;
using Model;
/// <summary>
///活动奖励信息业务逻辑
/// </summary>
/// 
namespace BL
{

    public class ActivityAwardInformationBL
    {
        ActivityAwardInformationService activityAwardInformationService = new ActivityAwardInformationService();
        public ActivityAwardInformationBL()
        {

        }

        //根据档案编号获取所有活动奖励信息业务逻辑
        public IList<ActivityAwardInformation> GetActivityAwardInformationByStudentID(string studentID)
        {
            try
            {
                return activityAwardInformationService.GetActivityAwardInformationByStudentID(studentID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        

        //获取活动奖励信息业务逻辑
        public IList<ActivityAwardInformation> GetActivityAwardInformation()
        {
            try
            {
                return activityAwardInformationService.GetActivityAwardInformation();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        //根据学号得到客房实体对象
        public ActivityAwardInformation GetActivityAwardInformationByID(string  awardID)
        {
            try
            {
                return activityAwardInformationService.GetActivityAwardInformationByID(awardID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        //根据奖励ID删除活动奖励信息
        public void  DeleteActivityAwardInformationByID(string  id)
        {
            
            try
            {
              activityAwardInformationService.DeleteActivityAwardInformationByID(id);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }



        //修改活动奖励信息业务逻辑
        public int  ModifyActivityAwardInformation(ActivityAwardInformation activityAwardInformation)
        {
            try
            {
               return  activityAwardInformationService.ModifyActivityAwardInformation(activityAwardInformation);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        //增加活动奖励信息业务逻辑
        public int   AddActivityAwardInformation(ActivityAwardInformation activityAwardInformation)
        {
            try
            {
               return   activityAwardInformationService.AddActivityAwardInformation(activityAwardInformation);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
        
    }
}
