﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Model;
using DA;

/// <summary>
/// ArchivesDestinationRecordBL 的摘要说明
/// </summary>
/// 
namespace BL
{
    public class ArchivesDestinationRecordBL
    {
        ArchivesDestinationRecordService archivesDestinationRecordService = new ArchivesDestinationRecordService();

        public ArchivesDestinationRecordBL()
        {

        }

        //根据学号得到档案去向信息集合
        public IList<ArchivesDestinationRecord> GetArchivesDestinationRecordByStudentID(string studentID)
        {
            try
            {
                return archivesDestinationRecordService.GetArchivesDestinationRecordByStudentID(studentID);
            }
            catch(Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        /// <summary>
        /// 根据去向ID得到档案去向记录实体对象
        /// </summary>
        /// <returns></returns>
        /// 
        public ArchivesDestinationRecord GetAllArchivesDestinationRecordByID(int  id)
        {
            try
            {
                return archivesDestinationRecordService.GetAllArchivesDestinationRecordByDestinationID(id);
            }
            catch(Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        //得到档案去向记录信息集合
        public IList<ArchivesDestinationRecord> GetArchivesDestinationRecord()
        {
            try
            {
                return archivesDestinationRecordService.GetArchivesDestinationRecord();
            }
            catch(Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        public void DeleteArchivesDestinationRecord(string   id)
        {
            try
            {
                archivesDestinationRecordService.DeleteArchivesDestinationRecord(id);
            }
            catch(Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        public int  ModifyArchivesDestinationRecord(ArchivesDestinationRecord archivesDestinationRecord)
        {
            try
            {
               return  archivesDestinationRecordService.ModifyArchivesDestinationRecord(archivesDestinationRecord);
            }
            catch(Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        public int AddArchivesDestinationRecord(ArchivesDestinationRecord archivesDestinationRecord)
        {
            try
            {
                return archivesDestinationRecordService.AddArchivesDestinationRecord(archivesDestinationRecord);
            }
            catch(Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        
    }
}