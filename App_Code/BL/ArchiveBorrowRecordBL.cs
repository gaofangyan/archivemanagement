﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using DA;
using Model;
/// <summary>
/// ArchiveBorrowRecord_BL 的摘要说明
/// </summary>
/// 
namespace BL
{
    public class ArchiveBorrowRecordBL
    {
        public ArchiveBorrowRecordService archiveBorrowRecordService = new ArchiveBorrowRecordService();
        public ArchiveBorrowRecordBL()
        {

        }
        //根据借阅ID得到档案借阅信息集合
        public IList<ArchiveBorrowRecord> GetArchiveBorrowRecordByAwardID(string   awardID)
        {
            try
            {
                return archiveBorrowRecordService.GetArchiveBorrowRecordByAwardID(awardID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        //得到档案借阅记录信息集合
        public IList<ArchiveBorrowRecord> GetArchiveBorrowRecord()
        {
            try
            {
                return archiveBorrowRecordService.GetArchiveBorrowRecord();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
        //根据学号得到档案借阅记录
        public IList<ArchiveBorrowRecord> GetArchiveBorrowRecordByStudentID(string studentID)
        {
            
            try
            {
                return  archiveBorrowRecordService.GetArchiveBorrowRecordByStudentID(studentID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        //根据ID得到档案借阅记录实体对象
        public ArchiveBorrowRecord GetArchiveBorrowRecordByID(string  id)
        {
            try
            {
                return archiveBorrowRecordService.GetArchiveBorrowRecordByID(id);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
        //修改档案借阅记录
        public int ModifyArchiveBorrowRecord(ArchiveBorrowRecord archiveBorrowRecord)
        {
            try
            {
                return archiveBorrowRecordService.ModifyArchiveBorrowRecord(archiveBorrowRecord);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        //增加档案借阅记录
        public int AddArchiveBorrowRecord(ArchiveBorrowRecord archiveBorrowRecord)
        {
            try
            {
                return archiveBorrowRecordService.AddArchiveBorrowRecord(archiveBorrowRecord);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        //删除档案借阅记录信息
        public void DeleteArchiveBorrowRecordByBorrowID(string  id)
        {
            try
            {
                archiveBorrowRecordService.DeleteArchiveBorrowRecordByID(id);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

       
    }
}