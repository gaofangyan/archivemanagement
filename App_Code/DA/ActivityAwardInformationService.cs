﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Text;
using Model;
using DB;

/// <summary>
/// Activity_Award_informationService 的摘要说明
/// </summary>
/// 
namespace DA
{
    public class ActivityAwardInformationService
    {
        string sql = string.Empty;

        public ActivityAwardInformationService()
        {
        }

        //根据学号得到活动奖励信息
        public IList<ActivityAwardInformation> GetActivityAwardInformationByStudentID(string studentID)
        {
            IList<ActivityAwardInformation> activityAwardInformations = new List<ActivityAwardInformation>();
            sql = "select * from ActivityAwardInformation ";
            if (studentID.ToString() != "0")
            {
                sql += "where StudentID=" + studentID.ToString();
            }
            using (SqlDataReader dr = DB.DBHelper.Exc(sql))
            {
                while (dr.Read())
                {
                    ActivityAwardInformation activityAwardInformation = new ActivityAwardInformation();
                    //ArchiveInformation archiveInfarmation = new ArchiveInformation();
                    activityAwardInformation.AwardID = Convert.ToString(dr["AwardID"]);
                    activityAwardInformation.AwardDiscription = Convert.ToString(dr["AwardDiscription"]);
                    activityAwardInformation.AwardType = Convert.ToString(dr["AwardType"]);
                    activityAwardInformations.Add(activityAwardInformation);
                }
            }
            return activityAwardInformations;
        }

        //得到活动奖励信息集合
        public IList<ActivityAwardInformation> GetActivityAwardInformation()
        {
            IList<ActivityAwardInformation> activityAwardInformations = new List<ActivityAwardInformation>();
            sql = "select * from ActivityAwardInformation ";

            using (SqlDataReader dr = DB.DBHelper.Exc(sql))
            {
                while (dr.Read())
                {
                    ActivityAwardInformation activityAwardInformation = new ActivityAwardInformation();
                    activityAwardInformation.AwardID = Convert.ToString(dr["AwardID"]);
                    activityAwardInformation.StudentID = Convert.ToString(dr["StudentID"]);
                    activityAwardInformation.AwardDiscription = Convert.ToString(dr["AwardDiscription"]);
                    activityAwardInformation.AwardType = Convert.ToString(dr["AwardType"]);
                    activityAwardInformations.Add(activityAwardInformation);
                    
                }
            }
            return activityAwardInformations;
        }

        //根据奖励ID得到活动奖励实体对象
        public ActivityAwardInformation GetActivityAwardInformationByID(string   awardID)
        {
            ActivityAwardInformation activityAwardInformation = new ActivityAwardInformation();
            sql = "select * from ActivityAwardInformation where AwardID=" + awardID.ToString();
            using (SqlDataReader dr = DB.DBHelper.Exc(sql))
            {
                while (dr.Read())
                {
                    activityAwardInformation.StudentID = Convert.ToString(dr["StudentID"]);
                    activityAwardInformation.AwardDiscription = Convert.ToString(dr["AwardDiscription"]);
                    activityAwardInformation.AwardType = Convert.ToString(dr["AwardType"]);
                }
            }
            return activityAwardInformation;
        }

           //根据奖励ID删除活动奖励信息
        public void DeleteActivityAwardInformationByID(string  id) 
        {
            sql = "delete from ActivityAwardInformation where AwardID=" + Convert.ToString(id);
            DB.DBHelper.ExecuteCommand(sql);
            
        }

        //修改活动奖励信息
        public int  ModifyActivityAwardInformation(ActivityAwardInformation activityAwardInformation)
        {
            int id;
            StringBuilder updateActivityAwardInformation = new StringBuilder();
            updateActivityAwardInformation.Append("update ActivityAwardInformation set");
            updateActivityAwardInformation.Append("AwardDiscription="+"'"+activityAwardInformation.AwardDiscription+"'");
            updateActivityAwardInformation.Append("AwardType=" + "'" + activityAwardInformation.AwardType + "'");
            updateActivityAwardInformation.Append("where AwardID=" + activityAwardInformation.AwardID);
            sql = updateActivityAwardInformation.ToString();
            id=DB.DBHelper.ExecuteCommand(sql);
            return id;
        }
        /// <summary>
        /// 增加活动奖励信息
        /// </summary>
        /// <returns></returns>
        public int   AddActivityAwardInformation(ActivityAwardInformation activityAwardInformation)
        {
            
            int id=0;
            StringBuilder addActivityAwardInformation = new StringBuilder();
            addActivityAwardInformation.Append("insert into ActivityAwardInformation(AwardID,StudentID,AwardDiscription,AwardType) values(" +activityAwardInformation.AwardID+ "','"
                +activityAwardInformation.StudentID+"','" + activityAwardInformation.AwardDiscription+ "','"+activityAwardInformation.AwardType+"')select @@IDENTITY");
            sql = addActivityAwardInformation.ToString();
            id = DB.DBHelper.ExecuteCommand(sql);
            return id;
           
        }
        
    }
}