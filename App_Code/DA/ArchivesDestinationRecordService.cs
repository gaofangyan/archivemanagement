﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Text;
using System.Configuration;
using DB;
using Model;

/// <summary>
/// ArchivesDestinationRecordService 的摘要说明
/// </summary>
/// 
namespace DA
{
    public class ArchivesDestinationRecordService
    {
        string sql = string.Empty;
        ArchiveInformation archiveInformation = new ArchiveInformation();
        public ArchivesDestinationRecordService()
        {

        }

        //根据学号得到档案去向信息集合
        public IList<ArchivesDestinationRecord> GetArchivesDestinationRecordByStudentID(string studentID)
        {
            IList<ArchivesDestinationRecord> archivesDestinationRecords = new List<ArchivesDestinationRecord>();
            sql = "select * from ArchivesDestinationRecord";
            if (studentID.ToString() != null)
            {
                sql += "where StudentID =" + studentID.ToString();
            }
            SqlDataReader dr = DB.DBHelper.Exc(sql);
            while (dr.Read())
            {
                ArchivesDestinationRecord archivesDestinationRecord = new ArchivesDestinationRecord();
                archivesDestinationRecord.DestinationID = Convert.ToInt32(dr["DestinationID"]);
                archivesDestinationRecord.StudentID = Convert.ToString(dr["StudentID"]);
                archivesDestinationRecord.StudentName = Convert.ToString(dr["StudentName"]);
                archivesDestinationRecord.ReceivingUnit = Convert.ToString(dr["RecivingUnit"]);
                archivesDestinationRecord.ReceivingUnitPhone = Convert.ToString(dr["ReceivingUnitPhone"]);
                archivesDestinationRecord.ReceivingTime = Convert.ToDateTime(dr["ReceivingTime"]);
                archivesDestinationRecord.UnitAddress = Convert.ToString(dr["UnitAddress"]);
                archivesDestinationRecords.Add(archivesDestinationRecord);

            }
            return archivesDestinationRecords;
        }

        /// <summary>
        /// 根据去向ID得到档案去向记录实体对象
        /// </summary>
        /// <returns></returns>
        /// 
        public ArchivesDestinationRecord GetAllArchivesDestinationRecordByDestinationID(int  id)
        {
            ArchivesDestinationRecord archivesDestinationRecord = new ArchivesDestinationRecord();
            SqlDataReader dr = DB.DBHelper.Exc(sql);
            if (dr.Read())
            {
                //archivesDestinationRecord.ID = Convert.ToInt32(dr["ID"]);
                archivesDestinationRecord.StudentID = Convert.ToString(dr["StudentID"]);
                archivesDestinationRecord.StudentName = Convert.ToString(dr["StudentName"]);
                archivesDestinationRecord.ReceivingUnit = Convert.ToString(dr["ReceivingUnit"]);
                archivesDestinationRecord.ReceivingTime = Convert.ToDateTime(dr["ReceivingTime"]);
                archivesDestinationRecord.ReceivingUnitPhone = Convert.ToString(dr["ReceivingUnitPhone"]);
                archivesDestinationRecord.UnitAddress = Convert.ToString(dr["UnitAddress"]);

            }
            return archivesDestinationRecord;
        }

        //得到档案去向记录信息集合
        public IList<ArchivesDestinationRecord> GetArchivesDestinationRecord()
        {
            IList<ArchivesDestinationRecord> archivesDestinationRecords = new List<ArchivesDestinationRecord>();
            sql = "select * from ArchivesDestinationRecord";
            SqlDataReader dr = DB.DBHelper.Exc(sql);
            while (dr.Read())
            {
                ArchivesDestinationRecord archivesDestinationRecord = new ArchivesDestinationRecord();
                archivesDestinationRecord.DestinationID = Convert.ToInt32(dr["DestinationID"]);
                archivesDestinationRecord.StudentID= Convert.ToString(dr["StudentID"]);
                archivesDestinationRecord.StudentName = Convert.ToString(dr["StudentName"]);
                archivesDestinationRecord.ReceivingUnit = Convert.ToString(dr["ReceivingUnit"]);
                archivesDestinationRecord.ReceivingTime = Convert.ToDateTime(dr["ReceivingTime"]);
                archivesDestinationRecord.ReceivingUnitPhone = Convert.ToString(dr["ReceivingUnitPhone"]);
                archivesDestinationRecord.UnitAddress = Convert.ToString(dr["UnitAddress"]);

                archivesDestinationRecords.Add(archivesDestinationRecord);
            }
            return archivesDestinationRecords;
        }

        public void DeleteArchivesDestinationRecord(string    id)
        {
            sql = "delete from ArchivesDestinationRecord where DestinationID=" + Convert.ToString(id);
            DB.DBHelper.ExecuteCommand(sql);
        }

        public int  ModifyArchivesDestinationRecord(ArchivesDestinationRecord archivesDestinationRecord)
        {
            int id;
            StringBuilder updateArchivesDestinationRecord = new StringBuilder();
            updateArchivesDestinationRecord.Append("update ArchivesDestinationRecord set ReceivingUnit='" + archivesDestinationRecord.ReceivingUnit
                + "',StudentName='"+archivesDestinationRecord.StudentName+"',ReceivingTime='" + archivesDestinationRecord.ReceivingTime + "',ReceivingUnitPhone='" + archivesDestinationRecord.ReceivingUnitPhone
                + "',UnitAddress='" + archivesDestinationRecord.UnitAddress + "' where DestinationID="+archivesDestinationRecord.DestinationID);
            sql = updateArchivesDestinationRecord.ToString();
            id=DB.DBHelper.ExecuteCommand(sql);
            return id;

        }

        public int AddArchivesDestinationRecord(ArchivesDestinationRecord archivesDestinationRecord)
        {

            int id;
            StringBuilder addArchivesDestinationRecord = new StringBuilder();
            addArchivesDestinationRecord.Append("insert into ArchivesDestinationRecord(StudentID,StudentName,ReceivingUnit,ReceivingTime,ReceivingUnitPhone,UnitAddress)values('"
                + archivesDestinationRecord.StudentID+ "','"+archivesDestinationRecord.StudentName+"','"+ archivesDestinationRecord.ReceivingUnit + "','" + archivesDestinationRecord.ReceivingTime
                + "','" + archivesDestinationRecord.ReceivingUnitPhone + "','" + archivesDestinationRecord.UnitAddress + "') select @@IDENTITY");

            sql = addArchivesDestinationRecord.ToString();
            id = DB.DBHelper.ExecuteCommand(sql);
            return id;
        }

       
    }
}