﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using Model;
using DB;

/// <summary>
/// AdministratorService 的摘要说明
/// </summary>
/// 
namespace DA
{
    public class AdministratorService
    {

        string sql = string.Empty;
        public AdministratorService()
        {
           
        }

        //根据用户名得到用户密码
        public string GetPasswordByAdministratorID(string  administratorID)
        {
            string pwd = string.Empty;
            sql = "select Password from Administrator where AdministratorID=" + "'" + administratorID + "'";
            using (SqlDataReader dr = DB.DBHelper.Exc(sql))
            {
                if (dr.Read())
                {
                    pwd = Convert.ToString(dr["Password"]);
                }
            }
            return pwd;
        }

        //修改密码
        public int  ModifyPassword(Administrator administrator)
        {
            int id;
            StringBuilder updatePassward = new StringBuilder();
            updatePassward.Append("update Administrator set Password='"+administrator.Password+ "'where AdministratorID="+administrator.AdministratorID);
            sql = updatePassward.ToString();
            id = DB.DBHelper.ExecuteCommand(sql);
            return id;

        }

        //获取当前信息
        public IList<Administrator> GetAdministrator(string administratorID)   
        {
            IList<Administrator> GetAdministrator = new List<Administrator>();
            sql = "select * from Administrator where AdministratorID=" + administratorID.ToString();
            SqlDataReader dr = DB.DBHelper.Exc(sql);
            if (dr.Read())
            {
                Administrator administrator = new Administrator();
                administrator.AdministratorID = Convert.ToString(dr["AdministratorID"]);
                administrator.AdministratorName = Convert.ToString(dr["AdministratorName"]);
                administrator.Sex = Convert.ToString(dr["Sex"]);
                administrator.Password = Convert.ToString(dr["Passward"]);
                administrator.EMail = Convert.ToString(dr["EMail"]);

                GetAdministrator.Add(administrator);
            }
            return GetAdministrator;
        }
    }
}