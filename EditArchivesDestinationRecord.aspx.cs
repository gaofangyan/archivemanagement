﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Model;
using BL;

public partial class EditArchivesDestinationRecord : System.Web.UI.Page
{
    ArchivesDestinationRecord archivesDestinationRecord = new ArchivesDestinationRecord();

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        ArchivesDestinationRecordBL archivesDestinationRecordBL = new ArchivesDestinationRecordBL();
        archivesDestinationRecord.StudentName = Convert.ToString(this.xsxm.Text.Trim());
        archivesDestinationRecord.ReceivingUnit = Convert.ToString(this.jsdw.Text.Trim());
        archivesDestinationRecord.ReceivingUnitPhone = Convert.ToString(this.jsdwdh.Text.Trim());
        archivesDestinationRecord.UnitAddress = Convert.ToString(this.dwzd.Text.Trim());
        archivesDestinationRecord.ReceivingTime = Convert.ToDateTime(this.jssj.Text.Trim());
        int count;
        count = archivesDestinationRecordBL.ModifyArchivesDestinationRecord(archivesDestinationRecord);
        if (count == 1)
        {
            Page.Server.Transfer("ViewArchivesDestinationRecord.aspx");
        }
        else
        {
            ClientScript.RegisterStartupScript(this.GetType(),
                    "", "<script>alert('修改失败！');</script>"); return;
        }
    }

    protected void Button2_Click(object sender, EventArgs e)
    {
        this.Dispose();
        Response.Redirect("ViewArchiveDestinationRecord.aspx");
    }

    
}